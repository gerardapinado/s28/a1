// 3 & 4
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(res => res.json())
.then(res => {
    console.log(res)
    let arr = res.map(data => {
        return data.title
    })
    arr.forEach(data => console.log(data))
})

// 5 & 6
fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then(res => res.json())
.then(res => {
    console.log(res)
    console.log(`The item "${res.title}" on the list has a status of ${res.completed}`)
})
// 7
fetch(`https://jsonplaceholder.typicode.com/todos`, {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify ({
        completed: false,
        title: "Created To Do List Item",
        userId: 1
    })
})
.then(res => res.json())
.then(res => console.log(res))

// 8 & 9
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify ({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure",
        id: 1,
        status: "Pending",
        title: "Updated To Do List Item",
        userId: 1
    })
})
.then(res => res.json())
.then(res => console.log(res))

// 10 & 11
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify ({
        dateCompleted: "07/09/21",
        // description: "To update the my to do list with a different data structure",
        // id: 1,
        status: "Complete",
        // title: "Updated To Do List Item",
        // userId: 1
    })
})
.then(res => res.json())
.then(res => console.log(res))

// 12
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "DELETE",
    headers: {
        "Content-Type": "application/json"
    },
})
.then(res => res.json())
.then(res => console.log(res))

// 13
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    
})